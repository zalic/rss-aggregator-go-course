package main

import (
	"fmt"
	"net/http"
	"rss-reader/JSONResponse"
	"rss-reader/internal/database"
)

func (apiCfg *apiConfig) handlerGetPostsForUser(w http.ResponseWriter, r *http.Request, user database.User) {
	posts, err := apiCfg.DB.GetPostsForUser(r.Context(), database.GetPostsForUserParams{
		UserID: user.ID,
		Limit:  20,
	})

	if err != nil {
		JSONResponse.RespondWithError(w, http.StatusInternalServerError, fmt.Sprint("Failed to fetch posts", err))
		return
	}

	JSONResponse.RespondWithJSON(w, http.StatusOK, databasePostsToPost(posts))
}
