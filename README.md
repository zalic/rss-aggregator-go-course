# RSS feed aggregator

This is write-along for the [Go Programming – Golang Course with Bonus Projects](https://youtu.be/un6ZyFkqFKo)

Course codebase: https://github.com/bootdotdev/fcc-learn-golang-assets/

## setup

### SQLC

[sqlc](https://github.com/kyleconroy/sqlc): A SQL Compiler - generates type-safe code from SQL

- https://docs.sqlc.dev/en/stable/tutorials/getting-started-postgresql.html

```
go install github.com/kyleconroy/sqlc/cmd/sqlc@latest
```

### Goose

[Goose](https://github.com/pressly/goose) is a database migration tool.
Manage your database schema by creating incremental SQL changes or Go functions.

```
go install github.com/pressly/goose/v3/cmd/goose@latest
```

## database

```
docker-compose up
```
```bash
cd sql/schema
goose postgres postgres://admin:admin@localhost:54320/rssagg up
# or
goose -dir sql/schema postgres postgres://admin:admin@localhost:54320/rssagg up
# or use direnv with GOOSE_DRIVER, GOOSE_DBSTRING and GOOSE_MIGRATION_DIR

```
```
sqlc generate
```
