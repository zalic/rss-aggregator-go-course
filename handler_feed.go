package main

import (
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"net/http"
	"rss-reader/JSONResponse"
	"rss-reader/internal/database"
	"time"
)

func (apiCfg *apiConfig) handlerCreateFeed(w http.ResponseWriter, r *http.Request, user database.User) {
	type parameters struct {
		Name string `json:"name"`
		Url  string `json:"url"`
	}
	decoder := json.NewDecoder(r.Body)
	params := parameters{}
	err := decoder.Decode(&params)
	if err != nil {
		JSONResponse.RespondWithError(w, 400, fmt.Sprint("Error parsing JSON", err))
		return
	}

	feed, err := apiCfg.DB.CreateFeed(r.Context(), database.CreateFeedParams{
		ID:        uuid.New(),
		CreatedAt: time.Now().UTC(),
		UpdatedAt: time.Now().UTC(),
		Name:      params.Name,
		Url:       params.Url,
		UserID:    user.ID,
	})

	if err != nil {
		JSONResponse.RespondWithError(w, 500, fmt.Sprint("Failed to create feed", err))
		return
	}

	JSONResponse.RespondWithJSON(w, http.StatusOK, databaseFeedToFeed(feed))
}

func (apiCfg *apiConfig) handlerGetFeeds(w http.ResponseWriter, r *http.Request) {
	feeds, err := apiCfg.DB.GetFeeds(r.Context())

	if err != nil {
		JSONResponse.RespondWithError(w, http.StatusInternalServerError, fmt.Sprint("Failed to fetch feeds", err))
		return
	}

	JSONResponse.RespondWithJSON(w, http.StatusOK, databaseFeedsToFeeds(feeds))
}
