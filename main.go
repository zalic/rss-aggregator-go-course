package main

import (
	"database/sql"
	"github.com/go-chi/chi"
	"github.com/go-chi/cors"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"log"
	"net/http"
	"os"
	"rss-reader/internal/database"
	"time"
)

type apiConfig struct {
	DB *database.Queries
}

func main() {
	godotenv.Load()

	port := os.Getenv("PORT")
	if port == "" {
		port = "9000"
	}

	dbURL := os.Getenv("DB_URL")
	if dbURL == "" {
		log.Fatal("DB_URL is not found in env")
	}

	conn, err := sql.Open("postgres", dbURL)
	if err != nil {
		log.Fatal("Failed to connect with the database")
	}

	apiCfg := apiConfig{
		DB: database.New(conn),
	}

	go startScraping(apiCfg.DB, 10, time.Minute)

	router := chi.NewRouter()

	router.Use(cors.Handler(cors.Options{
		AllowedOrigins:   []string{"https://*", "http://*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: false,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	}))

	srv := &http.Server{
		Handler: router,
		Addr:    ":" + port,
	}

	v1Router := chi.NewRouter()
	v1Router.Get("/healthz", handlerReadiness)
	v1Router.Get("/err", handlerErr)
	// $ curl -X POST -d '{"name":"test"}' http://localhost:9000/v1/users
	// {
	//    "id":"58d065c5-1525-40e8-853f-a4f811004390",
	//    "created_at":"2023-07-16T10:05:25.627734Z",
	//    "updated_at":"2023-07-16T10:05:25.627735Z",
	//    "name":"test",
	//    "api_key":"f043af6d895ceb056b69e53e8e5ac09d2204c951a09bf3c0072e55e682ec7975"
	// }
	v1Router.Post("/users", apiCfg.handlerCreateUser)
	// curl -H "Authorization: ApiKey <KEY>" http://localhost:9000/v1/users
	v1Router.Get("/users", apiCfg.middlewareAuth(apiCfg.handlerGetUserByAPIKey))
	// curl -X POST -d '{"name":"test", "url": "http://localhost"}' -H "Authorization: ApiKey <KEY>" http://localhost:9000/v1/feeds
	// {
	//    "ID":"20e70cd3-167c-4886-a826-55460220133b",
	//    "CreatedAt":"2023-07-16T15:51:21.284883Z",
	//    "UpdatedAt":"2023-07-16T15:51:21.284884Z",
	//    "Name":"test",
	//    "Url":"http://localhost",
	//    "UserID":"bbbf7746-08f0-41d0-9546-fa5f6ffe0e8f"
	// }
	v1Router.Post("/feeds", apiCfg.middlewareAuth(apiCfg.handlerCreateFeed))
	v1Router.Get("/feeds", apiCfg.handlerGetFeeds)
	v1Router.Get("/follow", apiCfg.middlewareAuth(apiCfg.handlerGetFeedFollows))
	// curl -X POST -d '{"feed_id":"<FEED_ID>"}' -H "Authorization: ApiKey <KEY>" http://localhost:9000/v1/follow
	v1Router.Post("/follow", apiCfg.middlewareAuth(apiCfg.handlerCreateFeedFollow))
	// curl -X DELETE -H "Authorization: ApiKey <KEY>" http://localhost:9000/v1/follow/<FEED-ID>
	v1Router.Delete("/follow/{feedId}", apiCfg.middlewareAuth(apiCfg.handlerDeleteFeedFollow))

	v1Router.Get("/posts", apiCfg.middlewareAuth(apiCfg.handlerGetPostsForUser))

	router.Mount("/v1", v1Router)

	// this is fine too
	router.Get("/h", handlerReadiness)
	// this is fine too
	router.Get("/h/e", handlerReadiness)

	// using GET when only POST handler is registered will result in "405 Method Not Allowed"
	router.Post("/h/post", handlerReadiness)

	// router.HandleFunc to handle any request type

	log.Println("Server starting on port", port)

	// ListenAndServe blocks, so nothing after this should ever be executed
	if err := srv.ListenAndServe(); err != nil {
		log.Fatal("Failed to start HTTP server:", err)
	}
}
