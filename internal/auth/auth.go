package auth

import (
	"errors"
	"net/http"
	"strings"
)

// GetAPIKey extract API Key form Authorization header
// Example: Authorization: ApiKey <KEY>
func GetAPIKey(headers http.Header) (string, error) {
	val := headers.Get("Authorization")
	if val == "" {
		return "", errors.New("Authorization header missing")
	}

	vals := strings.Split(val, " ")

	if len(vals) != 2 {
		return "", errors.New("Malformed Authorization")
	}

	if vals[0] != "ApiKey" {
		return "", errors.New("Unsupported Authorization")
	}

	return vals[1], nil
}
