package main

import (
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi"
	"github.com/google/uuid"
	"github.com/lib/pq"
	"net/http"
	"rss-reader/JSONResponse"
	"rss-reader/internal/database"
	"time"
)

func (apiCfg *apiConfig) handlerCreateFeedFollow(w http.ResponseWriter, r *http.Request, user database.User) {
	type parameters struct {
		FeedId uuid.UUID `json:"feed_id"`
	}
	decoder := json.NewDecoder(r.Body)
	params := parameters{}
	err := decoder.Decode(&params)
	if err != nil {
		JSONResponse.RespondWithError(w, 400, fmt.Sprint("Error parsing JSON", err))
		return
	}

	follow, err := apiCfg.DB.CreateFeedFollow(r.Context(), database.CreateFeedFollowParams{
		ID:        uuid.New(),
		CreatedAt: time.Now().UTC(),
		UpdatedAt: time.Now().UTC(),
		FeedID:    params.FeedId,
		UserID:    user.ID,
	})

	if err != nil {

		// is this how we do error handling?
		if pqErr, ok := err.(*pq.Error); ok {
			switch pqErr.Code.Name() {
			case "unique_violation":
				JSONResponse.RespondWithError(w, 400, fmt.Sprint("You are already following this feed"))
				return
			case "foreign_key_violation":
				JSONResponse.RespondWithError(w, 400, fmt.Sprint("Invalid feed-id"))
				return
			}
		}

		JSONResponse.RespondWithError(w, 500, fmt.Sprint("Failed to follow feed ", err))
		return
	}

	JSONResponse.RespondWithJSON(w, http.StatusOK, databaseFeedFollowToFeedFollow(follow))
}

func (apiCfg *apiConfig) handlerGetFeedFollows(w http.ResponseWriter, r *http.Request, user database.User) {
	follows, err := apiCfg.DB.GetFeedFollow(r.Context(), user.ID)

	if err != nil {
		JSONResponse.RespondWithError(w, http.StatusInternalServerError, fmt.Sprint("Failed to fetch followed feeds list", err))
		return
	}

	JSONResponse.RespondWithJSON(w, http.StatusOK, databaseFeedFollowsToFeedFollows(follows))
}

func (apiCfg *apiConfig) handlerDeleteFeedFollow(w http.ResponseWriter, r *http.Request, user database.User) {
	feedIdStr := chi.URLParam(r, "feedId")

	feedId, err := uuid.Parse(feedIdStr)

	if err != nil {
		JSONResponse.RespondWithError(w, 400, "Invalid Feed ID")
		return
	}

	err = apiCfg.DB.DeleteFeedFollow(r.Context(), database.DeleteFeedFollowParams{
		UserID: user.ID,
		FeedID: feedId,
	})

	if err != nil {
		JSONResponse.RespondWithError(w, 500, fmt.Sprint("Failed to unfollow feed ", err))
		return
	}

	JSONResponse.RespondWithJSON(w, http.StatusOK, map[string]string{"status": "ok"})
}
