package main

import (
	"fmt"
	"net/http"
	"rss-reader/JSONResponse"
	"rss-reader/internal/auth"
	"rss-reader/internal/database"
)

type authenticatedHandler func(w http.ResponseWriter, r *http.Request, user database.User)

func (apiCfg *apiConfig) middlewareAuth(handler authenticatedHandler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		apiKey, err := auth.GetAPIKey(r.Header)
		if err != nil {
			JSONResponse.RespondWithError(w, http.StatusForbidden, fmt.Sprint("Auth err: ", err))
			return
		}

		user, err := apiCfg.DB.GetUserByAPIKey(r.Context(), apiKey)

		if err != nil {
			JSONResponse.RespondWithError(w, http.StatusForbidden, "Invalid ApiKey")
			return
		}

		handler(w, r, user)
	}
}
