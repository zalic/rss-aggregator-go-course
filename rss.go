package main

import (
	"encoding/xml"
	"io"
	"net/http"
	"time"
)

/*
more details about parsing xml:
https://pkg.go.dev/encoding/xml#Unmarshal

descend into the XML structure
	<Group>
		<Value>Friends</Value>
		<Value>Squash</Value>
	</Group>

	Groups  []string `xml:"Group>Value"`

read attributes
	<Email where='work'>
		<Addr>gre@work.com</Addr>
	</Email>

	type Email struct {
		Where string `xml:"where,attr"`
		Addr  string
	}
*/

type RSSFeed struct {
	Channel struct {
		Title       string    `xml:"title"`
		Link        string    `xml:"link"`
		Description string    `xml:"description"`
		Language    string    `xml:"language"`
		Item        []RSSItem `xml:"item"`
	} `xml:"channel"`
}

type RSSItem struct {
	Title       string `xml:"title"`
	Link        string `xml:"link"`
	Description string `xml:"description"`
	PubDate     string `xml:"pubDate"`
}

func urlToFeed(url string) (RSSFeed, error) {
	rss := RSSFeed{}

	httpClient := http.Client{
		Timeout: 10 * time.Second,
	}

	resp, err := httpClient.Get(url)
	if err != nil {
		return rss, err
	}

	data, err := io.ReadAll(resp.Body)

	err = xml.Unmarshal(data, &rss)

	if err != nil {
		return rss, err
	}

	return rss, nil
}
