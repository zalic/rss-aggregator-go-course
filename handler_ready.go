package main

import (
	"net/http"
	"rss-reader/JSONResponse"
)

func handlerReadiness(w http.ResponseWriter, r *http.Request) {
	JSONResponse.RespondWithJSON(w, http.StatusOK, map[string]string{"status": "ok"})
}

func handlerErr(w http.ResponseWriter, r *http.Request) {
	JSONResponse.RespondWithError(w, http.StatusInternalServerError, "Internal Server Error")
}
